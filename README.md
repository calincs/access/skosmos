# Docker deployment for Skosmos

This is a Skosmos Docker image and deployment project customised for LINCS and based on Docker resources from the official project on [GitHub](https://github.com/NatLibFi/Skosmos). It connects to the LINCS vocabulary triple store at `https://fuseki.lincsproject.ca/skosmos/sparql`.

## Run Skosmos locally

Make sure you have Docker Desktop or Rancher Desktop installed along with Git.

```bash
git clone git@gitlab.com:calincs/access/skosmos.git
cd skosmos
docker compose up
```

Open the Skosmos UI at [http://localhost:9090](http://localhost:9090)
