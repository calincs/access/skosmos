FROM ubuntu:22.04

LABEL maintainer="LINCS"
LABEL version="0.1"
LABEL description="A LINCS customised Docker image for Skosmos with Apache"

ARG DEBIAN_FRONTEND=noninteractive
ENV SKOSMOS_VERSION=2.18

# git is necessary for some composer packages e.g. davidstutz/bootstrap-multiselect
# gettext is necessary as php-gettext was available in 18.04, but not in 20.04
RUN apt-get update && apt-get install -y \
    apache2 \
    curl \
    wget \
    tar \
    gettext \
    git \
    libapache2-mod-php8.1 \
    locales \
    php8.1 \
    php8.1-curl \
    php8.1-xsl \
    php8.1-intl \
    php8.1-mbstring \
    php-apcu \
    php-zip \
    unzip \
    sendmail \
    mailutils \
    openssl \
 && rm -rf /var/lib/apt/lists/*

# https://stackoverflow.com/a/28406007
# fixes warnings like perl: warning: Setting locale failed.
RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    locale-gen \
        en_US.utf8 \
        fr_FR.utf8
ENV LANGUAGE=en_US:en  
ENV LC_ALL=en_US.UTF-8 
ENV LANG=en_US.UTF-8  

# timezone
RUN sed -i 's!;date.timezone =!date.timezone = "America/Toronto"!g' /etc/php/8.1/apache2/php.ini

COPY config/000-default.conf /etc/apache2/sites-available/000-default.conf

RUN a2enmod rewrite
RUN a2enmod expires

# set ServerName & redirect error log to stderr for docker logs
RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf && \
  sed -ri \
  -e 's!^(\s*ErrorLog)\s+\S+!\1 /proc/self/fd/1!g' \
  "/etc/apache2/apache2.conf"

# download and extract Skosmos
WORKDIR /var/www/html
RUN wget https://github.com/NatLibFi/Skosmos/archive/refs/tags/v${SKOSMOS_VERSION}.tar.gz
RUN tar zxf v${SKOSMOS_VERSION}.tar.gz \
  && rm v${SKOSMOS_VERSION}.tar.gz \
  && mv Skosmos-${SKOSMOS_VERSION} Skosmos

# Composer and packages
WORKDIR /var/www/html/Skosmos
RUN curl -sS https://getcomposer.org/installer | php
RUN php composer.phar install --no-dev

# configure Skosmos
COPY config/config.ttl /var/www/html/Skosmos/config.ttl

# Copy CSS
COPY view/custom.css /var/www/html/Skosmos/resource/css/custom.css

# Copy other
COPY view/left.inc /var/www/html/Skosmos/view/left.inc
COPY view/about.inc /var/www/html/Skosmos/view/about.inc
COPY view/footer.inc /var/www/html/Skosmos/view/footer.inc

# adding favicon based on: https://github.com/NatLibFi/Skosmos/wiki/Configuration#custom-faviconico 
COPY static/custom-favicon.ico /var/www/html/Skosmos/custom-favicon.ico 

# disable web crawlers
COPY static/robots.txt /var/www/html/robots.txt 

# Copying plugin
ADD plugins/ /var/www/html/Skosmos/plugins/

# Copy other image assets
COPY static/lincs.png /var/www/html/Skosmos/resource/pics/lincs.png
COPY static/Logo.png /var/www/html/Skosmos/resource/pics/Logo.png

# redirect root requests to Skosmos
RUN rm /var/www/html/index.html
RUN echo "<script language="javascript">location.href='/Skosmos';</script>" >>/var/www/html/index.html
# add health check endpoint
RUN echo "<!DOCTYPE html><html lang='en'><head><meta charset='utf-8'><title>Health Check</title></head><body>OK</body></html>" >>/var/www/html/health.html

EXPOSE 80

# setup email
COPY config/entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]

CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]