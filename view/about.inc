{# This is the template for the about page. You can enter any html here and show
different language versions by using the lang value in a twig conditional as
demonstrated below. #} 
{% if request.lang == 'fr' %}

{% elseif request.lang == 'en' %}

<h1>About</h1>
<p>
</p>
<p>
</p>

<h2>Links</h2>
<ul>
  <li>
    <a
      href="https://portal.lincsproject.ca/docs/get-started/linked-open-data-basics/concepts-vocabularies"
      >LINCS Vocabularies Documentation</a
    >
  </li>
  <li>
    <a href="https://gitlab.com/calincs/access/skosmos">LINCS Skosmos GitLab repository</a>
  </li>
  <li>
    <a href="http://github.com/NatLibFi/Skosmos">Skosmos GitHub repository</a>
  </li>
</ul>

{% endif %}
