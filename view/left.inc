<div class="welcome-box">
<img src="resource/pics/lincs.png"/>
  {% if request.lang == 'en' %}
  <div>
  <p>LINCS vocabularies support the linking of data related to cultural identities and activities, broadly defined, to complement other linked data vocabularies. They aim to support diversity, inclusivity, and context in the representation of human culture online.
  </p>
  <p>These <a href="https://www.w3.org/TR/skos-reference/">SKOS</a> vocabularies are created by researchers associated with the <a href="https://lincsproject.ca/">Linked Infrastructure for Networked Cultural Scholarship</a> when existing vocabulary terms are unable to provide the semantic specificity required by a dataset. They are open for use by others, and LINCS welcomes further <a href="https://vocab.lincsproject.ca/Skosmos/en/feedback">contributions, collaboration</a>, and reuse.</p>
  </div>
  {% elseif request.lang == 'fr' %}
  <div>
  </div>
  {% endif %}
</div>