#!/bin/sh

# sendmail needs this to not time out on dns
# remove the first two lines (contains existing localhost)
tail -n +3 /etc/hosts > tmp && mv tmp /etc/hosts
# add new localhost line
echo "127.0.0.1 localhost.localdomain localhost $(hostname)" >> /etc/hosts

# add hostname to the localhost names file of sendmail
# remove the duplicate localhost line
tail -n +2 /etc/mail/local-host-names > tmp && mv tmp /etc/mail/local-host-names
# add the hostname
echo "$(hostname)" >> /etc/mail/local-host-names

# remove the MAILER lines from the sendmail config (will replace them)
grep -v -w 'MAILER(' /etc/mail/sendmail.mc > temp && mv temp /etc/mail/sendmail.mc

cat << EOF >> /etc/mail/sendmail.mc
define(\`SMART_HOST', \`[smtp.gmail.com]')dnl
define(\`RELAY_MAILER_ARGS', \`TCP \$h 587')dnl
define(\`ESMTP_MAILER_ARGS', \`TCP \$h 587')dnl
define(\`confAUTH_OPTIONS', \`A p y')dnl
define(\`confAUTH_MECHANISMS', \`EXTERNAL GSSAPI DIGEST-MD5 CRAM-MD5 LOGIN PLAIN')dnl
TRUST_AUTH_MECH(\`EXTERNAL DIGEST-MD5 CRAM-MD5 LOGIN PLAIN')dnl
FEATURE(\`authinfo', \`hash -o /etc/mail/auth/client-info.db')dnl
MAILER(\`local')dnl
MAILER(\`smtp')dnl
EOF

mkdir -p /etc/mail/auth
echo "AuthInfo:smtp.gmail.com \"U:lincs.project@gmail.com\" \"P:$EMAIL_PASSWORD\"" >> /etc/mail/auth/client-info
chmod 600 /etc/mail/auth/client-info
makemap hash /etc/mail/auth/client-info < /etc/mail/auth/client-info
make -C /etc/mail
service sendmail restart

exec "$@"

# test email: echo "Test email" | mail -s "Subject Here" zacanbot@gmail.com
# test email: echo "Subject: Test email" | sendmail -v zacanbot@gmail.com
