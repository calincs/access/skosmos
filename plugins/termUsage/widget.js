// declaring a namespace for the plugin
var TERM_USAGE = TERM_USAGE || {};

TERM_USAGE = {
  // variables in this namespace won't conflict with other global variables

  generateQueryString: function (uri) {
    const query = `
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX lincs: <http://www.id.lincsproject.ca/>
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
    PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
    SELECT ?graph (COUNT(distinct *) AS ?occurrenceCount) 
    WHERE   
    {
      GRAPH ?graph {
        bind( <${uri}> as ?o)
        {
          ?s ?p ?o.
        } 
        UNION 
        {
          ?o ?p ?s
        } 
      }
      FILTER(CONTAINS(STR(?graph), "http://graph.lincsproject.ca/") )
    }
    GROUP BY ?graph
    ORDER BY DESC(?occurrenceCount)

    `
    return query
  },

  sendQuery: function (query) {
    const url = "https://fuseki.lincsproject.ca/lincs/sparql";
    const queryUrl = url + "?query=" + encodeURIComponent(query);
    const queryHeaders = { Accept: "application/sparql-results+json" };
    return $.ajax({
      headers: queryHeaders,
      url: queryUrl,
      dataType: "json",
    }).done(
      function (response) {
        // Store the results
        TERM_USAGE.cache.results = response.results.bindings;
        TERM_USAGE.render(TERM_USAGE.displayResults(TERM_USAGE.cache.results));
      }.bind(this)
    );
  },

  cache: {
    results: null,
    clear: function () {
      this.results = null;
    },
  },
  displayResults: function (results) {
    // const results = response.results.bindings;
    if (results.length === 0) {
      return null
    }

    let html = `<div class="term-usage">
    <table>  
    <tr>
          <th>Dataset</th>
          <th>Counts of Use</th>
        </tr>`;

    results.forEach(result => {
      const graph = result.graph.value;
      const occurrenceCount = result.occurrenceCount.value;
      const graphName = graph.split("/").pop().split(".")[0];
      const graphLink = `<a href="https://portal.lincsproject.ca/docs/explore-lod/project-datasets/${graphName}" target="_blank">${graphName}</a>`;

      html += `
        <tr>
          <td>${graphLink}</td>
          <td>${occurrenceCount}</td>
        </tr>
      `;
    });
    html += "</table></div>";
    return html;
  },

  // likewise, functions here also won't conflict with other existing functions
  render: function (data) {
    if (data === null) {
      return;
    }
    
    var source = $("#termUsage-template").html();
    var template = Handlebars.compile(source);
    // if you want to place plugin below concept or vocabulary info use this instead:
    $("#content-bottom").append(template());
    $("#content-bottom").append(data);
  },
};

$(function () {
  // call a function in the plugin namespace
  
  // avoid polluting global namespace, or use meaningful and less likely to collide names
  window.term_usage_table = function (data) {
    if (!data.uri) {
      return;
    }
    TERM_USAGE.cache.clear();
    const query = TERM_USAGE.generateQueryString(data.uri);
    TERM_USAGE.sendQuery(query);

  };
});
