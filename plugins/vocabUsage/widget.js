// declaring a namespace for the plugin
var VOCAB_USAGE = VOCAB_USAGE || {};

VOCAB_USAGE = {
  // variables in this namespace won't conflict with other global variables

  generateQueryString: function (uri) {
    const query = `
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX lincs: <http://www.id.lincsproject.ca/>
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
    PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
    PREFIX occupation: <http://id.lincsproject.ca/occupation/>

    SELECT ?graph (COUNT(distinct ?o) AS ?unique) (COUNT(*) AS ?count) (group_concat(distinct ?term; separator=", ") as ?terms)
    WHERE {
      GRAPH ?graph {

        BIND("${uri}" AS ?ns)
        {
          ?s crm:P2_has_type ?o.
        } 
        UNION 
        {
          ?o a crm:E55_Type 
        } 
        UNION
        {
          ?o a skos:Concept 
        }
        FILTER(strstarts(str(?o), ?ns))
        
      }
      
      FILTER(contains(str(?graph), "graph.lincsproject.ca"))
      BIND(REPLACE(str(?o), ?ns, "") AS ?term)
      
    } group by ?graph
    ORDER BY DESC(?count)
    `;
    return query;
  },

  sendQuery: function (query) {
    const url = "https://fuseki.lincsproject.ca/lincs/sparql";
    const queryUrl = url + "?query=" + encodeURIComponent(query);
    const queryHeaders = { Accept: "application/sparql-results+json" };
    return $.ajax({
      headers: queryHeaders,
      url: queryUrl,
      dataType: "json",
    }).done(
      function (response) {
        // Store the results
        VOCAB_USAGE.cache.results = response.results.bindings;
        VOCAB_USAGE.render(
          VOCAB_USAGE.displayResults(VOCAB_USAGE.cache.results)
        );
      }.bind(this)
    );
  },

  cache: {
    results: null,
    clear: function () {
      this.results = null;
    },
  },
  displayResults: function (results) {
    if (results.length === 0) {
      return null;
    }

    let html = `<div class="vocab-usage">
    <table>  
    <tr>
          <th>Dataset</th>
          <th>Total Terms Used</th>
          <th>Unique Terms Used</th>
          <th>Terms Used</th>
        </tr>`;

    results.forEach((result) => {
      const graph = result.graph.value;
      const totalCount = result.count.value;
      const uniqueCount = result.unique.value;
      const termsUsed = result.terms.value;
      const graphName = graph.split("/").pop().split(".")[0];
      const graphLink = `<a href="https://portal.lincsproject.ca/docs/explore-lod/project-datasets/${graphName}" target="_blank">${graphName}</a>`;

      // Generating links for terms
      const page_url = `${window.location.href}page`;
      const termLinks = termsUsed
        .split(", ")
        .sort()
        .map((term) => {
          const termLink = `<a href="${page_url}/${term}">${term}</a>`;
          return termLink;
        })
        .join(", ");

      html += `
        <tr>
          <td>${graphLink}</td>
          <td>${totalCount}</td>
          <td>${uniqueCount}</td>
          <td>${termLinks}</td>
        </tr>
      `;
    });
    html += "</table></div>";
    return html;
  },

  // likewise, functions here also won't conflict with other existing functions
  render: function (data) {
    if (data === null) {
      return;
    }

    var source = $("#vocabUsage-template").html();
    var template = Handlebars.compile(source);
    // if you want to place plugin below concept or vocabulary info use this instead:
    // $("#vocab-info").append(template());
    // $("#vocab-info").append(data);
    $("#content-bottom").append(template());
    $("#content-bottom").append(data);
  },
};

$(function () {
  // call a function in the plugin namespace

  // avoid polluting global namespace, or use meaningful and less likely to collide names
  window.vocab_usage_table = function (data) {

    if (data.uri) {
      return;
    }

    if (window.location.href.includes("search?")) {
      return;
    }
    
    VOCAB_USAGE.cache.clear();
    const query = VOCAB_USAGE.generateQueryString(uriSpace);
    VOCAB_USAGE.sendQuery(query);
  };
});
